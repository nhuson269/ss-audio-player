//
//  SSAudioPlayer.swift
//  SSAudioPlayer
//
//  Created by Nhu Son on 5/25/18.
//  Copyright © 2018 Nhu Son. All rights reserved.
//

import AVFoundation

enum AVPlayerState {
    case Playing
    case Paused
    case Reserved
    case Unknown
}

@objc protocol SSAudioPlayerDelegate: class {
    
    /* Player did updated duration time
     * You can get duration time of audio in here
     */
    func playerDidUpdateDurationTime(player: SSAudioPlayer, durationTime: CMTime)
    
    /* Player did change time playing
     * You can get current time play of audio in here
     */
    func playerDidUpdateCurrentTimePlaying(player: SSAudioPlayer, currentTime: CMTime)
    
    // Player begin start
    func playerDidStart(player: SSAudioPlayer)
    
    // Player paused
    func playerDidPaused(player: SSAudioPlayer)
    
    // Player did finish playing
    func playerDidFinishPlaying(player: SSAudioPlayer)
    
    // Player did fail play
    func playerDidFailPlay(player: SSAudioPlayer)
}

class SSAudioPlayer: NSObject {
    // MARK: - Variable
    var timer = Timer()
    var state: AVPlayerState = .Unknown
    var audioPlayerItem: AVPlayerItem?
    var audioPlayer = AVPlayer()
    var currentAudioPath: URL!
    var delegate : SSAudioPlayerDelegate!
    
    @objc func failedToPlay() {
        print("error play")
        self.cancelTimer()
        state = .Paused
        self.audioPlayer.pause()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.audioPlayerItem)
        if self.delegate != nil {
            self.delegate.playerDidFailPlay(player: self)
        }
    }
    
    func configWithAudioURLString(url: String) {
        if let urlAudio = URL(string: url) {
            self.audioPlayerItem = AVPlayerItem(url: urlAudio)
            NotificationCenter.default.addObserver(self, selector: #selector(failedToPlay), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.audioPlayerItem)
            self.audioPlayer = AVPlayer(playerItem: self.audioPlayerItem)
            self.audioPlayer.rate = 1.0;
            self.audioPlayer.addObserver(self, forKeyPath: "rate", options: [.old, .new], context: nil)
            self.audioPlayer.pause()
        } else {
            fatalError("This class does not support Url")
        }
    }
    
    init (url: String) {
        super.init()
        self.configWithAudioURLString(url: url)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    deinit {
        self.audioPlayer.removeObserver(self, forKeyPath: "rate", context: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.audioPlayerItem)
    }
    
    // stop timer
    func cancelTimer() {
        timer.invalidate()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if let rate = change?[NSKeyValueChangeKey.newKey] as? Float {
                if rate == 0.0 {
                    print("playback paused")
                    // Call delegate
                    if self.delegate != nil {
                        self.delegate.playerDidPaused(player: self)
                    }
                    
                    // Cancel timer update laber title
                    self.cancelTimer()
                    state = .Paused
                }
                
                if rate == 1.0 {
                    print("normal playback")
                    // Call delegate
                    if self.delegate != nil {
                        self.delegate.playerDidStart(player: self)
                        self.delegate.playerDidUpdateDurationTime(player: self, durationTime: (self.audioPlayer.currentItem?.asset.duration)!)
                    }
                    
                    // Begin update label time
                    self.startTimer()
                    state = .Playing
                }
                
                if rate == -1.0 {
                    print("reverse playback")
                    state = .Reserved
                }
            }
        }
    }
    
    @objc func timerAction() {
        // Call delegate
        if self.audioPlayer.currentItem!.asset.duration == kCMTimeZero {
            self.failedToPlay()
            return
        }
        
        if (CMTimeGetSeconds(self.audioPlayer.currentItem!.asset.duration) - CMTimeGetSeconds(self.audioPlayer.currentTime()) < 1 && self.delegate != nil) {
            self.delegate.playerDidFinishPlaying(player: self)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.audioPlayerItem)
        }
        
        if self.delegate != nil {
            self.delegate.playerDidUpdateCurrentTimePlaying(player: self, currentTime: (self.audioPlayer.currentItem?.asset.duration)!)
        }
    }
    
    // MARK: - Timer update status of player
    func startTimer() {
        timer.invalidate() // just in case this button is tapped multiple times
        
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    // MARK: - Property & action
    func play() {
        if (self.audioPlayer.currentItem != nil) && (state != .Playing) {
            if self.audioPlayer.currentItem?.currentTime().durationText == self.audioPlayer.currentItem?.asset.duration.durationText {
                self.audioPlayer.seek(to: kCMTimeZero)
            }
            
            self.audioPlayer.play()
        }
    }
    
    func pause() {
        if (self.audioPlayer.currentItem != nil) && (state == .Playing)  {
            self.audioPlayer.pause()
        }
    }
    
    func playedTimeStr() -> String {
        return CMTimeMakeWithSeconds(CMTimeGetSeconds(self.audioPlayer.currentItem!.asset.duration) - CMTimeGetSeconds(self.audioPlayer.currentTime()), 1).durationText
    }
    
    func durationStr() -> String {
        return (self.audioPlayer.currentItem?.asset.duration.durationText)!
    }
    
    func rate() -> Float {
        return Float(CMTimeGetSeconds(self.audioPlayer.currentTime()) / CMTimeGetSeconds(self.audioPlayer.currentItem!.asset.duration))
    }
    
    func sliderValueDidChange(value: Float) {
        let seekTime = CMTimeMakeWithSeconds(Double(value) * CMTimeGetSeconds(self.audioPlayer.currentItem!.asset.duration), 1);
        self.audioPlayer.seek(to: seekTime)
    }
}

//MARK: CMTime extension
extension CMTime {
    var durationText:String {
        let totalSeconds = CMTimeGetSeconds(self)
        let hours: Int = Int(totalSeconds / 3600)
        let minutes: Int = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
        let seconds: Int = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}
