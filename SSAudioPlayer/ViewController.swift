//
//  ViewController.swift
//  SSAudioPlayer
//
//  Created by Nhu Son on 5/25/18.
//  Copyright © 2018 Nhu Son. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, SSAudioPlayerDelegate {
    @IBOutlet weak var btPlay: UIButton!
    @IBOutlet weak var btPause: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lbTimePlayed: UILabel!
    @IBOutlet weak var activityLoad: UIActivityIndicatorView!
    @IBOutlet weak var lbTimeAudio: UILabel!
    
    var ssAudioPlayer: SSAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        ssAudioPlayer = SSAudioPlayer.init(url: "http://cms.bienhoc.vn/uploads/sound/abc.mp3")
        ssAudioPlayer?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func slider_ValueChanged(_ sender: Any) {
        if let mySlider = sender as? UISlider {
            ssAudioPlayer?.sliderValueDidChange(value: mySlider.value)
        }
    }
    
    @IBAction func btPlay_Clicked(_ sender: Any) {
        ssAudioPlayer?.play()
        activityLoad.startAnimating()
    }
    
    @IBAction func btPause_Clicked(_ sender: Any) {
        ssAudioPlayer?.pause()
    }
    
    func playerDidUpdateDurationTime(player: SSAudioPlayer, durationTime: CMTime) {
        DispatchQueue.main.async {
            self.lbTimeAudio.text = player.durationStr()
        }
    }
    
    func playerDidUpdateCurrentTimePlaying(player: SSAudioPlayer, currentTime: CMTime) {
        DispatchQueue.main.async {
            self.btPlay.isHidden = true
            self.btPause.isHidden = false
            self.lbTimePlayed.text = player.playedTimeStr()
            self.slider.value = player.rate()
            self.activityLoad.stopAnimating()
        }
    }
    
    func playerDidStart(player: SSAudioPlayer) {

    }
    
    func playerDidPaused(player: SSAudioPlayer) {
        DispatchQueue.main.async {
            self.btPlay.isHidden = false
            self.btPause.isHidden = true
            self.activityLoad.stopAnimating()
        }
    }
    
    func playerDidFinishPlaying(player: SSAudioPlayer) {
        DispatchQueue.main.async {
            self.btPlay.isHidden = false
            self.btPause.isHidden = true
            self.lbTimePlayed.text = "00:00"
            self.lbTimeAudio.text = "00:00"
            self.slider.value = 0
            self.activityLoad.stopAnimating()
        }
    }
    
    func playerDidFailPlay(player: SSAudioPlayer) {
        
    }
}

